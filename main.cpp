#include <iostream>
#include "BST.h"

void printBST(BST& tmp){
	for (BST::Iterator i = tmp.begin(); i != tmp.end(); i++) {
		std::pair<const int&, double&> s = *i;
		std::cout << s.first << " " << s.second << std::endl;
	}
}



int main(){
	std::cout << "BST test" << std::endl;
	BST a;
	std::cout << "Add key = 30 value = 10.5f" << std::endl;
	a.insert(30, 10.5f);
	std::cout << "Add key = 20 value = 12.54f" << std::endl;
	a.insert(20, 12.54f);
	std::cout << "Add key = 19 value = 100.54f" << std::endl;
	a.insert(19, 100.54f);
	std::cout << "Add key = 22 value = 200" << std::endl;
	a.insert(22, 200);
	std::cout << "Add key = 50 value = 2020.179f" << std::endl;
	a.insert(50, 2020.179f);
	std::cout << "Max = " << a.max()->value << std::endl;
	std::cout << "Min = " << a.min()->value << std::endl;

	std::cout << "find(20) = " << (a.find(20)? "Yes" : "No") << std::endl;
	std::cout << "Deleting the key = 30" << std::endl;
	a.remove(30);
	std::cout << "Deleting the key = 22" << std::endl;
	a.remove(22);
	std::cout << "We get the head" << std::endl;

	BST::Node* head = a.max(); 
	std::cout << "Conclusion head->key = " << head->key << std::endl;
	std::cout << "Conclusion head->value = " << head->value << std::endl;
	std::cout << "Print BST" << std::endl;
	printBST(a);
	std::cout << "Deleting everything" << std::endl;
	a.remove(20);
	a.remove(19);
	a.remove(50);
	head = a.max(); 
	std::cout << "checking for emptiness = " << ( !head ? "No" : "Yes") << std::endl;
	return 0;
}
