#pragma once

#include <utility>
#include <cstddef>


typedef int KeyType;
typedef double ValueType;


class BST {
public:
    BST() = default;    
 
    explicit BST(const BST& other);
    BST& operator=(const BST& other);

    explicit BST(BST&& other) noexcept;
    BST& operator=(BST&& other) noexcept;

    ~BST();

    struct Node {
        KeyType key;        //!< ключ
        ValueType value;    //!< значение
        Node* left;         //!< указатель на элемент левого поддерева
        Node* right;        //!< указатель на элемент правого поддерева
        Node* parent;	    //!< указатель на родителя
    };

    //! Итератор бинарного дерева поиска
    class Iterator
    {
    public:
        explicit Iterator(Node* ptr);
        
	std::pair<const KeyType&, ValueType&> operator*() ;
        const std::pair<const KeyType&, ValueType&> operator*() const;
    
	Node* operator->();
        const Node* operator->() const;

        Iterator& operator++();
        Iterator operator++(int);
	
	Iterator& operator--();
        Iterator operator--(int);

        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;

    private:
        Node* _ptr;
    };
    void insert(KeyType key, ValueType value);
    void remove(KeyType key);
    Node* find(KeyType key) const;
    Node* min() const;
    Node* max() const;
    Iterator root() const;
    Iterator begin() const;
    Iterator end() const;
    size_t size() const;
private:
    size_t _size = 0;
    Node* _root = nullptr; //!< корневой узел дерева
};
