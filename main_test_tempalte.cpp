#include <iostream>

#include "BinarySearchTree.h"



int main() {
	std::cout << "Test multi map" << std::endl;
	BinarySearchTree<int, int> myltimap;
	myltimap.insert(10, 11);
	myltimap.insert(11, 121);
	myltimap.insert(9, 93);

	
	for (auto i = myltimap.begin(); i != myltimap.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}
	myltimap.insert(9, 100);

	std::cout << std::endl;
	
	for (auto i = myltimap.begin(); i != myltimap.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}
	
	std::cout << std::endl;

	
	myltimap.erase(10);

	for (auto i = myltimap.begin(); i != myltimap.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}

	std::cout << (myltimap.find(9) != myltimap.end() ? "Yes element 9" : "Not element 9") << std::endl;


	auto max = myltimap.max();
	auto min = myltimap.min();

	std::cout << "Max = a[" << (*max).first << "] : " << (*max).second << std::endl;
	std::cout << "Min = a[" << (*min).first << "] : " << (*min).second << std::endl;
	
	std::cout << "Size = " << myltimap.size() << std::endl << std::endl;
	
	myltimap.insert(11, 1);

	for (auto i = myltimap.begin(); i != myltimap.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}


	std::pair<BinarySearchTree<int, int>::Iterator, BinarySearchTree<int, int>::Iterator> a = myltimap.equalRange(11);
	
	std::cout << (myltimap.find(11) != myltimap.end() ? "Yes element" : "Not element") << std::endl;

	for (auto i = a.first ; i != a.second; i--) {
		std::cout << "myltimap[" << (*i).first << "] = "<< (*i).second << std::endl;
	}
	
	std::cout << "Сopy verification myltimap" << std::endl;
	BinarySearchTree<int, int> myltimap1(myltimap);
	
	for (auto i = myltimap.begin(); i != myltimap.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}
	
	std::cout << std::endl;

	std::cout << "Delete myltimap1 " << std::endl;
	myltimap1.~BinarySearchTree<int, int>();
	
	std::cout << "Movements verification" << std::endl;
	myltimap1 = std::move(myltimap);
	
	for (auto i = myltimap1.begin(); i != myltimap1.end(); i++) {
		std::cout << "myltimap[" <<  (*i).first << "] = " << (*i).second << std::endl;
	}
	myltimap1.~BinarySearchTree<int, int>();

	std::cout << "Test Map" << std::endl;

	Map<int, char> map;

	map[10] = 'o';

	std::cout << map[10] << std::endl;
	map.insert(11, 's');
	map.insert(9, 's');

	for ( auto i = map.begin(); i != map.end(); i++) {
		std::cout << "map["<< i->first << "] = " << i->second << std::endl;
	}
	std::cout << std::endl;

	map.erase(10);
	
	std::cout << (map.find(10) != map.end() ? "Yes element 10" : "Not element 10") << std::endl;
	std::cout << (map.find(9) != map.end() ? "Yes element 9" : "Not element 9") << std::endl;
	std::cout << "Size map = " << map.size() << std::endl << std::endl;
	
	for ( auto i = map.cbegin(); i != map.cend(); i++) {
		std::cout << "map["<< i->first << "] = " << i->second << std::endl;
	}

	std::cout << "Сopy verification Map" << std::endl;

	Map<int, char> map1(map);


	for ( auto i = map1.cbegin(); i != map1.cend(); i++) {
		std::cout << "map["<< i->first << "] = " << i->second << std::endl;
	}
	map1.~Map<int, char>();

	std::cout << "Delete map1 " << std::endl;
	std::cout << "Size map1 = " << map1.size() << std::endl;


	std::cout << "Test multi set" << std::endl;

	Set<int> set;
	set.insert(10);
	set.insert(110);
	set.insert(10);
	set.insert(11);
	
	std::cout << "Print set" << std::endl;
	for ( auto i = set.begin(); i != set.end(); i++) {
		std::cout << i->first << " ";
	}
	std::cout << std::endl << set.contains(10) << std::endl;
	std::cout << set.contains(0) << std::endl;
	set.~Set<int>();


	return 0;
}

