#include "BST.h"

static void freeBST(BST::Node* other) {
	if ( !other ) {
		return;
	}
	freeBST(other->left);
	freeBST(other->right);
	delete other;
}

static BST::Node* cpBST(BST::Node* other) {
	if ( !other ) {
		return other;
	} 
	BST::Node* tmpRoot = new BST::Node;
	tmpRoot->key = other->key;
	tmpRoot->value = other->value;
	tmpRoot->parent = nullptr;
	tmpRoot->left = cpBST(other->left);
	tmpRoot->right = cpBST(other->right);
	if ( tmpRoot->left ) {
		tmpRoot->left->parent = tmpRoot;
	}
	if ( tmpRoot->right ) {
		tmpRoot->right->parent = tmpRoot;
	}
	return tmpRoot;
}


static BST::Node* findBST(BST::Node* other, KeyType key) {
	if ( !other || other->key == key ) {
		return other;
	}
	return findBST( (other->key > key ? other->left : other->right), key );
}

static BST::Node* maxBST(BST::Node* other) {
	if (!other || !other->right){
		return other;
	}
	return maxBST(other->right);
}

static BST::Node* minBST(BST::Node* other) {
	if (!other || !other->left){
		return other;
	}
	return maxBST(other->left);
}

static std::pair<BST::Node*, bool> insertBST(BST::Node* other, KeyType key, ValueType value) {
 	// почему не юзаю поиск обесняю потом му что поис возращет сылку на эдемент а надо сылку на сылку точнее указаетль на указатель но так как у нас метод макс const мы не можем так зделать а писать новую функцию как то влом
	if ( !other ) {
		other = new BST::Node;
		other->key = key;
		other->value = value;
		other->left = nullptr;
		other->right = nullptr;
		other->parent = nullptr;
		return std::make_pair(other, true);
	}
	if ( other->key == key ) {
		other->value = value;
		return std::make_pair(other, false);
	}
	if ( other->key > key ) {
		std::pair<BST::Node*, bool> tmpElemt = insertBST(other->left, key, value);
		other->left = tmpElemt.first;
		tmpElemt.first->parent = other;
		return std::make_pair(other, tmpElemt.second);
	}
	std::pair<BST::Node*, bool> tmpElemt = insertBST(other->right, key, value);
	other->right = tmpElemt.first;
	tmpElemt.first->parent = other;
	return std::make_pair(other, tmpElemt.second);
}
 
static std::pair<BST::Node*, bool> removeBST(BST::Node* other, KeyType key) {
	if ( !other ) {
		return std::make_pair(other, false);
	}
	if ( other->key == key && (!other->left && !other->right) ){
		delete other;
		return std::make_pair(nullptr, true);
	}
	if ( other->key == key && ( !other->left || !other->right ) ){
		BST::Node* tmp = other->left ? other->left : other->right;
		tmp->parent = other->parent;
		delete other;
		return std::make_pair(tmp, true);
	}
	if ( other->key == key ) {
		BST::Node* tmp = other->left;
		BST::Node* parent = nullptr;
		while ( tmp->right ) { // почему не макс поесняю надо ссылку возращать а макс указаетль а надо указатель на указатель
			parent = tmp;
			tmp = tmp->right;
		}
		other->key = tmp->key;
		other->value = tmp->value;
		(parent ? parent->right : other->left) = removeBST(tmp, tmp->key).first;
		return std::make_pair(other, true);
	}
	// тоже самое с поиском
	if (key > other->key){
		std::pair<BST::Node*, bool> tmp = removeBST(other->right, key);
		other->right = tmp.first;
		return std::make_pair(other, tmp.second);
	}
	std::pair<BST::Node*, bool> tmp = removeBST(other->left, key);
	other->right = tmp.first;
	return std::make_pair(other, tmp.second);
}

BST::BST(const BST& other) {
	*this = other;
}

BST& BST::operator=(const BST& other) {
	if ( &other == this ) {
		return *this;
	}
	this->_root = cpBST(other._root);
	return *this;
}

BST::BST(BST&& other) noexcept {
	*this = std::move(other);
}

BST& BST::operator=(BST&& other) noexcept {
	if ( &other == this ) {
		return *this;
	}
	this->~BST();
	this->_root = other._root;
	this->_size = other._size;
	other._root = nullptr;
	other._size = 0;
	return * this;
}

BST::~BST() {
	freeBST(this->_root);
	this->_root = nullptr;
	this->_size = 0;
}

BST::Iterator::Iterator(BST::Node* ptr) : _ptr(ptr) {

} 

std::pair<const KeyType&, ValueType&> BST::Iterator::operator*() {
	std::pair<const KeyType&, ValueType&> tmp = {this->_ptr->key, this->_ptr->value};
	return  tmp;
}

const std::pair<const KeyType&, ValueType&> BST::Iterator::operator*() const {
	std::pair<const KeyType&, ValueType&> tmp = {this->_ptr->key, this->_ptr->value};
	return  tmp;
}

BST::Node* BST::Iterator::operator->() {
	return this->_ptr;
}

const BST::Node* BST::Iterator::operator->() const {
	return this->_ptr;
}

BST::Iterator& BST::Iterator::operator++() {
	if ( this->_ptr->right ) {
 	      this->_ptr = minBST(this->_ptr->right);
	      return *this;
	}
	BST::Node* parent = this->_ptr->parent;
	while ( parent && this->_ptr == parent->right ) { 
		this->_ptr = parent;
		parent = parent->parent;
	}
	this->_ptr = parent;
	return *this;
}

BST::Iterator BST::Iterator::operator++(int) {
	BST::Node* ret = this->_ptr;
	if ( this->_ptr->right ) {
 	      this->_ptr = minBST(this->_ptr->right);
	      return BST::Iterator (ret);
	}
	BST::Node* parent = this->_ptr->parent;
	while ( parent && this->_ptr == parent->right ) { 
		this->_ptr = parent;
		parent = parent->parent;
	}
	this->_ptr = parent;
	return BST::Iterator (ret);
}

BST::Iterator& BST::Iterator::operator--() {
	if ( this->_ptr->left ) {
		this->_ptr = maxBST(this->_ptr->left);
		return *this;
	}
	BST::Node* parent = this->_ptr->parent;
	while ( parent && this->_ptr == parent->left ) {
		this->_ptr = parent;
		parent = parent->parent;
	}
	this->_ptr = parent;
	return *this;
}

BST::Iterator BST::Iterator::operator--(int) {
	BST::Node* ret = this->_ptr;
	if ( this->_ptr->left ) {
		this->_ptr = maxBST(this->_ptr->left);
                return BST::Iterator (ret);
	}
	BST::Node* parent = this->_ptr->parent;
	while ( parent && this->_ptr == parent->left ) {
		this->_ptr = parent;
		parent = parent->parent;
	}
	this->_ptr = parent;
	return BST::Iterator (ret);
}

bool BST::Iterator::operator==(const BST::Iterator& other) const { 
	return this->_ptr == other._ptr;
}

bool BST::Iterator::operator!=(const BST::Iterator& other) const { 
	return this->_ptr != other._ptr;
}

void BST::insert(KeyType key, ValueType value) {
 	std::pair<BST::Node*, bool> newAdd = insertBST(this->_root, key, value);
	this->_root = newAdd.first;
	this->_size += newAdd.second ? 1 : 0;
}

void BST::remove(KeyType key) {
	std::pair<BST::Node*, bool> rmElem = removeBST(this->_root, key);
	this->_root = rmElem.first;
	this->_size -= rmElem.second ? 1 : 0;
}

BST::Node* BST::find(KeyType key) const {
	return findBST(this->_root, key);
}

BST::Node* BST::max() const {
	return maxBST(this->_root);
}

BST::Node* BST::min() const {
	return minBST(this->_root);
}

BST::Iterator BST::root() const {
	return BST::Iterator (this->_root);
}

BST::Iterator BST::begin() const {
	return BST::Iterator ( minBST(this->_root) );
}

BST::Iterator BST::end() const {
	return ++BST::Iterator ( maxBST(this->_root) );
}

size_t BST::size() const {
	return this->_size;
}	
