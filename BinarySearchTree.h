#pragma once

#include <utility>
#include <cstddef>

/*!
    Имплементация бинарного дерева поиска
    Допускается дублирование ключей (аналог multimap)
*/
template <typename Key, typename Value>
class BinarySearchTree
{
    struct Node
    {
        Node(Key key, 
             Value value, 
             Node* parent = nullptr, 
             Node* left = nullptr, 
             Node* right = nullptr) : parent(parent), left(left), right(right) {
		keyValuePair = std::make_pair(key, value);
	}

        std::pair<Key, Value> keyValuePair;
        Node* parent = nullptr;
        Node* left = nullptr;
        Node* right = nullptr;
    };

public:
    BinarySearchTree() = default;
    
    explicit BinarySearchTree(const BinarySearchTree& other);
    BinarySearchTree& operator=(const BinarySearchTree& other);

    explicit BinarySearchTree(BinarySearchTree&& other) noexcept;
    BinarySearchTree& operator=(BinarySearchTree&& other) noexcept;

    ~BinarySearchTree();

    class Iterator
    {
    public:
        explicit Iterator(Node* node);

        std::pair<Key, Value>& operator*();
        const std::pair<Key, Value>& operator*() const;

        std::pair<Key, Value>* operator->();
        const std::pair<Key, Value>* operator->() const;

        Iterator operator++();
        Iterator operator++(int);

        Iterator operator--();
        Iterator operator--(int);

        bool operator==(const Iterator& other) const;
        bool operator!=(const Iterator& other) const;

    private:
        Node* _node;
    };

    class ConstIterator
    {
    public:
        explicit ConstIterator(const Node* node);

        const std::pair<Key, Value>& operator*() const;

        const std::pair<Key, Value>* operator->() const;

        ConstIterator operator++();
        ConstIterator operator++(int);

        ConstIterator operator--();
        ConstIterator operator--(int);

        bool operator==(const ConstIterator& other) const;
        bool operator!=(const ConstIterator& other) const;

    private:
        const Node* _node;

    };

    void insert(const Key& key, const Value& value);
    void erase(const Key& key);

    ConstIterator find(const Key& key) const;
    Iterator find(const Key& key);

    std::pair<Iterator, Iterator> equalRange(const Key& key);
    std::pair<ConstIterator, ConstIterator> equalRange(const Key& key) const;

    ConstIterator min() const;
    ConstIterator max() const;
    Iterator begin();
    Iterator end();
    ConstIterator cbegin() const;
    ConstIterator cend() const;

    size_t size() const;
private:
    size_t _size = 0;
    Node* _root = nullptr; //!< корневой узел дерева
    void freeBST(Node* root);
    Node* cpBST(Node* root);
    static Node* minBST(Node* root);
    static Node* maxBST(Node* root);
    static Node* findBST(Node* root, Key other);
    std::pair<Node*, bool> removeBST(Node* root, Key other);
    std::pair<Node*, bool> insertBST(Node* root, Key key, Value value);
};


template<typename Key, typename Value>
void BinarySearchTree<Key, Value>::freeBST(BinarySearchTree<Key, Value>::Node* root) {
	if (!root) {
		return;
	}
	this->freeBST(root->left);
	this->freeBST(root->right);
	delete root;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Node* BinarySearchTree<Key, Value>::cpBST(BinarySearchTree<Key, Value>::Node* root) {
	if (!root) {
		return root;
	}
	BinarySearchTree<Key, Value>::Node* tmpRoot = new BinarySearchTree<Key, Value>::Node(root->keyValuePair.first, root->keyValuePair.second);
	tmpRoot->left = this->cpBST(root->left);
	tmpRoot->right = this->cpBST(root->right);
	if ( tmpRoot->left ) {
		tmpRoot->left->parent = tmpRoot;
	}
	if ( tmpRoot->right ) {
		tmpRoot->right->parent = tmpRoot;
	}
	return tmpRoot;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Node* BinarySearchTree<Key, Value>::minBST(BinarySearchTree<Key, Value>::Node* root) {
	if ( !root || !root->left ) {
		return root;
	}
	return BinarySearchTree<Key, Value>::minBST(root->left);
}

template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Node* BinarySearchTree<Key, Value>::maxBST(BinarySearchTree<Key, Value>::Node* root) {
	if ( !root || !root->right ) {
		return root;
	}
	return BinarySearchTree<Key, Value>::maxBST(root->right);
}

template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Node*  BinarySearchTree<Key, Value>::findBST(Node* root, Key other) { 
	if ( !root || root->keyValuePair.first == other ) {
		return root;
	}
	return BinarySearchTree<Key, Value>::findBST(root->keyValuePair.first < other ? root->right : root->left, other);
}


template<typename Key, typename Value>
std::pair<typename BinarySearchTree<Key, Value>::Node*, bool> BinarySearchTree<Key, Value>::insertBST(Node* root, Key key, Value value) { 
	if ( !root ) {
		root = new BinarySearchTree<Key, Value>::Node(key, value);
		return std::make_pair(root, true);
	}
	if ( root->keyValuePair.first >= key ) {
		std::pair<BinarySearchTree<Key, Value>::Node*, bool> tmp = this->insertBST(root->left, key, value);
		root->left = tmp.first;
		tmp.first->parent = root;
		return std::make_pair(root, tmp.second);
	}
	std::pair<BinarySearchTree<Key, Value>::Node*, bool> tmp = this->insertBST(root->right, key, value);
	root->right = tmp.first;
	tmp.first->parent = root;
	return std::make_pair(root, tmp.second);
}


template<typename Key, typename Value>
std::pair<typename BinarySearchTree<Key, Value>::Node*, bool> BinarySearchTree<Key, Value>::removeBST(Node* root, Key key) { 
	if ( !root ) {
		return std::make_pair(root, false);
	}
	if ( root->keyValuePair.first == key && (!root->left && !root->right) ) {
		delete root;
		return std::make_pair(nullptr, true);
	}
	if ( root->keyValuePair.first == key && ( !root->left || !root->right ) ) {
		BinarySearchTree<Key, Value>::Node* tmp = root->left ? root->left : root->right;
		tmp->parent = root->parent;
		delete root;
		return std::make_pair(tmp, true);
	}
	if ( root->keyValuePair.first == key ) {
		BinarySearchTree<Key, Value>::Node* tmp = root->left;
		BinarySearchTree<Key, Value>::Node* parent = nullptr;
		while ( tmp->right ) { // почему не макс поесняю надо ссылку возращать а макс указаетль а надо указатель на указатель
			parent = tmp;
			tmp = tmp->right;
		}
		root->keyValuePair.first = tmp->keyValuePair.first;
		root->keyValuePair.second = tmp->keyValuePair.second;
		(parent ? parent->right : root->left) = this->removeBST(tmp, tmp->keyValuePair.first).first;
		return std::make_pair(root, true);
	}
	// тоже самое с поиском
	if (key > root->keyValuePair.first){
		std::pair<BinarySearchTree<Key, Value>::Node*, bool> tmp = this->removeBST(root->right, key);
		root->right = tmp.first;
		return std::make_pair(root, tmp.second);
	}
	std::pair<BinarySearchTree<Key, Value>::Node*, bool> tmp = this->removeBST(root->left,key); 
	root->right = tmp.first;
	return std::make_pair(root, tmp.second);
}


template<typename Key, typename Value>
BinarySearchTree<Key, Value>::BinarySearchTree(const BinarySearchTree<Key, Value>& other) {
	if ( &other == this ) {
		return ;
	}
	*this = other;
}


template<typename Key, typename Value>
BinarySearchTree<Key, Value>& BinarySearchTree<Key, Value>::BinarySearchTree::operator=(const BinarySearchTree<Key, Value>& other) {
	if ( &other == this ) {
		return *this;
	}
	this->freeBST(this->_root);
	this->_size = 0;
	this->_root = this->cpBST(other._root);
	this->_size = other._size;
	return *this;
}
	

template<typename Key, typename Value>
BinarySearchTree<Key, Value>::BinarySearchTree(BinarySearchTree<Key, Value>&& other) noexcept {
	if ( &other == this ) {
		return ;
	}
	*this = std::move(other);
}


template<typename Key, typename Value>
BinarySearchTree<Key, Value>& BinarySearchTree<Key, Value>::operator=(BinarySearchTree<Key, Value>&& other) noexcept {
	if ( &other == this ) {
		return *this;
	}
	this->freeBST(this->_root);
	this->_size = 0;
	this->_root = other._root;
	this->_size = other._size;
	other._root = nullptr;
	other._size = 0;
	return *this;
}

template<typename Key, typename Value>
BinarySearchTree<Key, Value>::~BinarySearchTree() {
	this->freeBST(this->_root);
	this->_size = 0;
	this->_root = nullptr;
}

template<typename Key, typename Value>
BinarySearchTree<Key, Value>::Iterator::Iterator(BinarySearchTree<Key, Value>::Node* node) {
	this->_node = node;
}


template<typename Key, typename Value>
const std::pair<Key, Value>& BinarySearchTree<Key, Value>::Iterator::operator*() const {
	return this->_node->keyValuePair;
}


template<typename Key, typename Value>
std::pair<Key, Value>& BinarySearchTree<Key, Value>::Iterator::operator*() {
	return this->_node->keyValuePair;
}


template<typename Key, typename Value>
std::pair<Key, Value>* BinarySearchTree<Key, Value>::Iterator::operator->() {
	return &this->_node->keyValuePair;
}


template<typename Key, typename Value>
const std::pair<Key, Value>* BinarySearchTree<Key, Value>::Iterator::operator->() const {
	return &this->_node->keyValuePair; 

}


template<typename Key, typename Value>
bool BinarySearchTree<Key, Value>::Iterator::operator==(const BinarySearchTree<Key, Value>::Iterator& other) const {
       return this->_node == other._node;
}       


template<typename Key, typename Value>
bool BinarySearchTree<Key, Value>::Iterator::operator!=(const BinarySearchTree<Key, Value>::Iterator& other) const {
       return this->_node != other._node;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::Iterator::operator++() {
        if ( this->_node->right ) {
  	      this->_node = BinarySearchTree<Key, Value>::minBST(this->_node->right);
	      return *this;
	}
	BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->right ) { 
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return *this;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::Iterator::operator++(int) {
        BinarySearchTree<Key, Value>::Node* ret = this->_node;
        if ( this->_node->right ) {
 	      this->_node = BinarySearchTree<Key, Value>::minBST(this->_node->right);
	      return BinarySearchTree<Key, Value>::Iterator(ret);
        }
  	BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->right ) { 
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return BinarySearchTree<Key, Value>::Iterator(ret);
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::Iterator::operator--() {
	if ( this->_node->left ) {
		this->_node = BinarySearchTree<Key, Value>::maxBST(this->_node->left);
		return *this;
	}
  	BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->left ) {
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return *this;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::Iterator::operator--(int) {
        BinarySearchTree<Key, Value>::Node* ret = this->_node;
	if ( this->_node->left ) {
		this->_node = BinarySearchTree<Key, Value>::maxBST(this->_node->left);
		return  BinarySearchTree<Key, Value>::Iterator(ret);
	}
  	BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->left ) {
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return  BinarySearchTree<Key, Value>::Iterator(ret);
}


template<typename Key, typename Value>
size_t BinarySearchTree<Key, Value>::size() const {
	return this->_size;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::begin() {
	return BinarySearchTree<Key, Value>::Iterator (this->minBST(this->_root));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::end() {
	return ++BinarySearchTree<Key, Value>::Iterator (this->maxBST(this->_root));
}


template<typename Key, typename Value>
BinarySearchTree<Key, Value>::ConstIterator::ConstIterator(const BinarySearchTree<Key, Value>::Node* node) {
	this->_node = node;
}


template<typename Key, typename Value>
bool BinarySearchTree<Key, Value>::ConstIterator::operator==(const BinarySearchTree<Key, Value>::ConstIterator& other) const {
       return this->_node == other._node;
}       


template<typename Key, typename Value>
bool BinarySearchTree<Key, Value>::ConstIterator::operator!=(const BinarySearchTree<Key, Value>::ConstIterator& other) const {
       return this->_node != other._node;
}


template<typename Key, typename Value>
const std::pair<Key, Value>& BinarySearchTree<Key, Value>::ConstIterator::operator*() const {
	return this->_node->keyValuePair;
}


template<typename Key, typename Value>
const std::pair<Key, Value>* BinarySearchTree<Key, Value>::ConstIterator::operator->() const {
	return &this->_node->keyValuePair;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::ConstIterator::operator++() {
	if ( this->_node->right ) {
  	      this->_node = BinarySearchTree<Key, Value>::minBST(this->_node->right);
	      return *this;
	}
	const BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->right ) { 
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return *this;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::ConstIterator::operator++(int) {
	const BinarySearchTree<Key, Value>::Node* ret = this->_node;
        if ( this->_node->right ) {
 	      this->_node = BinarySearchTree<Key, Value>::minBST(this->_node->right);
	      return BinarySearchTree<Key, Value>::ConstIterator(ret);
        }
  	const BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->right ) { 
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return BinarySearchTree<Key, Value>::ConstIterator(ret);
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::ConstIterator::operator--() {
	if ( this->_node->left ) {
		this->_node = BinarySearchTree<Key, Value>::maxBST(this->_node->left);
		return *this;
	}
  	const BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->left ) {
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return *this;
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::ConstIterator::operator--(int) {
        const BinarySearchTree<Key, Value>::Node* ret = this->_node;
	if ( this->_node->left ) {
		this->_ptr = BinarySearchTree<Key, Value>::maxBST(this->_node->left);
		return  BinarySearchTree<Key, Value>::ConstIterator(ret);
	}
  	const BinarySearchTree<Key, Value>::Node* parent = this->_node->parent;
	while ( parent && this->_node == parent->left ) {
		this->_node = parent;
		parent = parent->parent;
	}
	this->_node = parent;
	return  BinarySearchTree<Key, Value>::ConstIterator(ret);
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::cbegin() const {
	return BinarySearchTree<Key, Value>::ConstIterator (this->minBST(this->_root));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::cend() const {
	return ++BinarySearchTree<Key, Value>::ConstIterator (this->maxBST(this->_root));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::min() const {
	return BinarySearchTree<Key, Value>::ConstIterator (this->minBST(this->_root));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::max() const {
	return BinarySearchTree<Key, Value>::ConstIterator (this->maxBST(this->_root));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::ConstIterator BinarySearchTree<Key, Value>::find(const Key& key) const {
	return BinarySearchTree<Key, Value>::ConstIterator (this->findBST(this->_root, key));
}


template<typename Key, typename Value>
typename BinarySearchTree<Key, Value>::Iterator BinarySearchTree<Key, Value>::find(const Key& key) {
	return BinarySearchTree<Key, Value>::Iterator (this->findBST(this->_root, key));
}


template<typename Key, typename Value>
void BinarySearchTree<Key, Value>::insert(const Key& key, const Value& value) {
	std::pair<BinarySearchTree<Key, Value>::Node*, bool> newRoot = this->insertBST(this->_root, key, value);
	this->_root = newRoot.first;
	this->_size += newRoot.second ? 1 : 0;
}


template<typename Key, typename Value>
void BinarySearchTree<Key, Value>::erase(const Key& key) {
	while ( true ) {
		std::pair<BinarySearchTree<Key, Value>::Node*, bool> newRoot = this->removeBST(this->_root, key);
		if ( !newRoot.second ) {
			break;
		}
		this->_size -= 1;
		this->_root = newRoot.first;
	}
}


template<typename Key, typename Value>
std::pair<typename BinarySearchTree<Key, Value>::Iterator, typename BinarySearchTree<Key, Value>::Iterator> BinarySearchTree<Key, Value>::equalRange(const Key& key) {
	BinarySearchTree<Key, Value>::Node* tindOther1 = this->findBST(this->_root, key);
	if ( !tindOther1 ) {
		return std::make_pair( BinarySearchTree<Key, Value>::Iterator (nullptr), BinarySearchTree<Key, Value>::Iterator (nullptr) );
	}
	BinarySearchTree<Key, Value>::Node* tindOther2 = nullptr;
	BinarySearchTree<Key, Value>::Node* endFirst = tindOther1;
	while ( (endFirst = this->findBST(endFirst->left, key) ) ) {
		tindOther2 = endFirst;
	}
	if ( tindOther2 ) {
		return std::make_pair( BinarySearchTree<Key, Value>::Iterator (tindOther1), --BinarySearchTree<Key, Value>::Iterator (tindOther2) );
	}
	return std::make_pair( BinarySearchTree<Key, Value>::Iterator (tindOther1), --BinarySearchTree<Key, Value>::Iterator (tindOther1) );
}


template<typename Key, typename Value>
std::pair<typename BinarySearchTree<Key, Value>::ConstIterator, typename BinarySearchTree<Key, Value>::ConstIterator> BinarySearchTree<Key, Value>::equalRange(const Key& key) const{
	BinarySearchTree<Key, Value>::Node* tindOther1 = this->findBST(this->_root, key);
	if ( tindOther1 ) {
		return std::make_pair( BinarySearchTree<Key, Value>::ConstIterator (nullptr), BinarySearchTree<Key, Value>::ConstIterator (nullptr) );
	}
	BinarySearchTree<Key, Value>::Node* tindOther2 = nullptr;
	BinarySearchTree<Key, Value>::Node* endFirst = tindOther1;
	while ( (endFirst = this->findBST(endFirst->left, key) ) ) {
		tindOther2 = endFirst;
	}
	if ( tindOther2 ) {
		return std::make_pair( BinarySearchTree<Key, Value>::ConstIterator (tindOther1), --BinarySearchTree<Key, Value>::ConstIterator (tindOther2) );
	}
	return std::make_pair( BinarySearchTree<Key, Value>::ConstIterator (tindOther1), --BinarySearchTree<Key, Value>::ConstIterator (tindOther1) );
}
	

/*!
    Имплементация словаря
    Не допускается дублирование ключей (аналог std::map)
*/
template <typename Key, typename Value>
class Map
{
    BinarySearchTree<Key, Value> _tree;
public:
    using MapIterator = typename BinarySearchTree<Key, Value>::Iterator;
    using ConstMapIterator = typename BinarySearchTree<Key, Value>::ConstIterator;

    Map() = default;
    
    explicit Map(const Map& other);
    Map& operator=(const Map& other);

    explicit Map(Map&& other) noexcept;
    Map& operator=(Map&& other) noexcept;

    ~Map() = default;

    void insert(const Key& key, const Value& value);

    void erase(const Key& key);

    ConstMapIterator find(const Key& key) const;
    MapIterator find(const Key& key);

    // доступ к элементу по ключу
    // если в момент обращения элемента не существует, создать его, 
    // ключ равен key, value равно дефолтному значению для типа Value
    //const Value& operator[](const Key& key) const; а вот это неляз  делать мы не выпоним условия const да в std::map на такое компиляор бранится очень неприличными словами :)
    Value& operator[](const Key& key);

    MapIterator begin();
    MapIterator end();

    ConstMapIterator cbegin() const;
    ConstMapIterator cend() const;

    size_t size() const;
};


template <typename Key, typename Value>
Map<Key, Value>::Map(const Map<Key, Value>& other) {
	*this = other;
}


template <typename Key, typename Value>
Map<Key, Value>& Map<Key, Value>::Map::operator=(const  Map<Key, Value>& other) {
	if ( this == &other ) {
		return *this;
	}
	this->_tree = other._tree;
	return *this;
}


template <typename Key, typename Value>
Map<Key, Value>::Map(Map<Key, Value>&& other) noexcept {
	if ( this == &other ) {
		return;
	}
	this->_tree = std::move(other._tree);
}


template <typename Key, typename Value>
Map<Key, Value>& Map<Key, Value>::Map::operator=(Map<Key, Value>&& other) noexcept {
	if ( this == &other ) {
		return *this;
	}
	this->_tree = std::move(other._tree);
	return *this;
}


template <typename Key, typename Value>
size_t Map<Key, Value>::Map::size() const {
	return this->_tree.size();
}


template <typename Key, typename Value>
typename Map<Key, Value>::MapIterator Map<Key, Value>::Map::begin() {
	return this->_tree.begin();
}


template <typename Key, typename Value>
typename Map<Key, Value>::MapIterator Map<Key, Value>::Map::end() {
	return this->_tree.end();
}


template <typename Key, typename Value>
typename Map<Key, Value>::ConstMapIterator Map<Key, Value>::Map::cbegin() const {
	return this->_tree.cbegin();
}


template <typename Key, typename Value>
typename Map<Key, Value>::ConstMapIterator Map<Key, Value>::Map::cend() const {
	return this->_tree.cend();
}


template <typename Key, typename Value>
typename Map<Key, Value>::ConstMapIterator Map<Key, Value>::Map::find(const Key& key) const {
	return this->_tree.find(key);
}
    

template <typename Key, typename Value>
typename Map<Key, Value>::MapIterator Map<Key, Value>::Map::find(const Key& key) {
	return this->_tree.find(key);
}


template <typename Key, typename Value>
void  Map<Key, Value>::Map::erase(const Key& key) {
	this->_tree.erase(key);
}


template <typename Key, typename Value>
void  Map<Key, Value>::insert(const Key& key, const Value& value) {
	Map<Key, Value>::MapIterator tmp = this->find(key);
	if ( tmp != Map<Key, Value>::MapIterator(nullptr) ) {
		tmp->second = value;
		return ;
	}
	this->_tree.insert(key, value);
}


template <typename Key, typename Value>
Value& Map<Key, Value>::operator[](const Key& key) {
	Map<Key, Value>::MapIterator tmp = this->find(key);
	while ( true) {
		Map<Key, Value>::MapIterator tmp = this->find(key);
		if ( tmp != Map<Key, Value>::MapIterator(nullptr) ) {
			return tmp->second;
		}
		this->_tree.insert(key, Value());
	}
}


/*!
    Имплементация множества
    Не допускается дублирование ключей (аналог std::set)
*/
template <typename Value>
class Set
{
    Map<Value, Value> _map;

public:
    using SetIterator = typename Map<Value, Value>::MapIterator;
    using ConstSetIterator = typename Map<Value, Value>::ConstMapIterator;

    Set() = default;

    explicit Set(const Set& other);
    Set& operator=(const Set& other);

    explicit Set(Set&& other) noexcept;
    Set& operator=(Set&& other) noexcept;

    ~Set() = default;

    void insert(const Value& value);

    void erase(const Value& value);

    ConstSetIterator find(const Value& value) const;
    SetIterator find(const Value& key);

    SetIterator begin();
    SetIterator end();

    ConstSetIterator cbegin() const;
    ConstSetIterator cend() const;

    bool contains(const Value& value) const;
};


template<typename Value> 
Set<Value>::Set(const Set<Value>& other) {
	*this = other;
}


template<typename Value> 
Set<Value>& Set<Value>::Set::operator=(const Set<Value>& other) {
	if ( this == &other ) {
		return *this;
	}
	this->_map = other._map;
	return *this;
}


template<typename Value>
Set<Value>::Set(Set<Value>&& other)  noexcept {
	if ( this == &other ) {
		return ;
	}
	this->_map = std::move(other._map);
}


template<typename Value>
Set<Value>&  Set<Value>::operator=(Set<Value>&& other) noexcept {
	if ( this == &other ) {
		return *this;
	}
	this->_map = std::move(other._map);
	return *this;
}


template<typename Value> 
void Set<Value>::Set::insert(const Value& value) {
	this->_map.insert(value, value);
}


template<typename Value> 
void Set<Value>::Set::erase(const Value& value) {
	this->_map.erase(value, value);
}


template<typename Value> 
typename Set<Value>::ConstSetIterator Set<Value>::Set::find(const Value& value) const {
	return this->_map.find(value);
}


template<typename Value> 
typename Set<Value>::SetIterator Set<Value>::Set::find(const Value& value)  {
	return this->_map.find(value);
}


template<typename Value> 
bool  Set<Value>::Set::contains(const Value& value) const {
	return this->find(value) != this->cend();
}


template<typename Value>
typename Set<Value>::SetIterator Set<Value>::Set::begin() {
	return this->_map.begin();
}

template<typename Value>
typename Set<Value>::SetIterator Set<Value>::Set::end() {
	return this->_map.end();
}


template<typename Value>
typename Set<Value>::ConstSetIterator Set<Value>::Set::cbegin() const {
	return this->_map.cbegin();
}

template<typename Value>
typename Set<Value>::ConstSetIterator Set<Value>::Set::cend() const {
	return this->_map.cend();
}
