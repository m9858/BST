.PHONY: all clear run

all: test

test: main.o BST.o
	g++ $^ -o testBST

%.o: %.cpp
	g++ -c $< -o $*.o

clear:
	rm -f  *.o testBST

run:test
	./testBST
