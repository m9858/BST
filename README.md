# Binary Search tree

### What is it all about?
In reference implementations of data structures, data is stored as separate elements linked by pointers. The elements are organized either as a sequence (list) or as a tree. Consider the organization of a reference data structure based on a binary search tree.

The binary search tree stores elements that can be compared with each other using the "less" and "more" operations, for example, these can be numbers or strings.

All the elements that are stored in the tree are unique, that is, one number cannot be written in two vertices of the tree. Each vertex of the tree has two subtree - left and right (they can be empty), and in the left subtree all elements are less than the value recorded at this vertex, and in the right subtree - more.


### What is implemented?

Еhe following functions are implemented in the class

```
insert(key, value) -> additions
remove(key) -> delete
max() -> max search
min() -> min seatch
find(key) -> value search
getRoot() -> bring back the head of the tree
```

### Run and compile

for the test there is **make** or black **cc** run do it with **./testBST**
or **make run**
